# supplementary statistics
source("./src/utils.R")

dt <- data.table(maindf)

#------------------------------------------------------------------------------#
# World map representation of the average of each variable (along the 4 years) #
#------------------------------------------------------------------------------#

params = VARIABLES[
  VARIABLES %!in% c("country","region","year","happyRank")
]

for(item in params){
  worldmap(maindf,item,paste(item," average 2015-2018"))
}

#-----------------------------------------------------------------------------------------------#
# Difference between national average & regional average of some variables, in active countries #
#-----------------------------------------------------------------------------------------------#

data = maindf[maindf$country %in% ACTIVE_COUNTRIES,]
params = c(
  "happyScore",
  "logGDP",
  "freedom",
  "corruptionPerception",
  "confidenceInGov",
  "democraticQuality"
)

for (p in params) {
  df_region = aggregate(x = maindf[,c(p)],by = list(region =maindf$region),FUN = mean)
  df_country =aggregate(x = data[,c(p)],by = list(country = data$country,region = data$region),FUN = mean)
  df_country$delta <- NA
  
  for(cnt in ACTIVE_COUNTRIES){
    reg = df_country[df_country$country==cnt,c("region")]
    reg_avg = df_region[df_region$region == reg,c("x")]
    count_avg = df_country[df_country$country==cnt,c("x")]
    df_country[df_country$country==cnt,c("delta")] = reg_avg - count_avg
  }
  
  barchart(df_country$country,df_country$delta,"","",paste(p," delta 2015-2018"))
}




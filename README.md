# R-project

Etude de l'évolution de paramètres issues du projet "World Happiness" et leur eventuel impact sur le déclanchement de mouvements socieaux 

# Pour ce projet :

 * Nous chercherons des relations entre des paramètres exploitables pouvant conduir à ce type d'évènements 

# Nous procéderons comme suit :

 * Définir l'ensemble des paramètres considérés pour notre étude
 
 * Etudier l'évolutions de l'ensemble de ces variables sur les quatres dernières années (2015-2018) : echelle mondiale et regionale

 * Choisir un ensemble d'exemples localisé (pays ayant récemment connu ce type de mouvement), et établir une comparaison entre l'évolution globale observée et celle des pays choisis.